# Demographic database for German YouTuber
This database contains information on 199 randomly scraped YouTube channels. All of them are registered in Germany.  The database is the part of a methodological project that concluded with this article in the Journal Frontiers in Big Data: https://www.frontiersin.org/articles/10.3389/fdata.2022.908636/full

## Contents
This database contains the following 3 .cvs files 
1. **YT_Channeldata** → The base dataset containing data scraped from YouTube 
2. **YT_Variables** → An overview of all variables used in the database
3. **YT_Values** → An Overview of numeric values used in the database and their string equivalent 

## Methods used
Data was obtained by accessing the YouTube API through the r-package TubeR. The list of channel names was sampled via the website channelcrawler.com.

For a more detailed explanation of how the data was obtained and how the different methods were used to estimate the gender of the channel owners, please see https://www.frontiersin.org/articles/10.3389/fdata.2022.908636/full

## Contact
The dataset as well as the corresponding article are part of the DFG-funded project TubeWork of the University Potsdam (https://www.uni-potsdam.de/de/sozialstrukturanalyse/index/forschung/tubework).
For help, advice or errors in the dataset please contact:
Prof. Dr. Roland Verwiebe via roland.verwiebe@uni-potsdam.de  
or Claudia Buder via cbuder@uni-potsdam.de
